Food Plus MOD
===========

Food Plus localization files.

-Add any localization files here to add language support for Food Plus.

-Use the en_US.lang file as the template of your entries, this is the most up-to-date file!

-NEVER EVER CHANGE en_US.lang or es_ES.lang. These are my files.

-Keep in mind these files HAVE to be UTF-8 without BOM encoded, anything else will NOT work!

-I recommend Notepad++! Don't use programs like Wordpad, Notepad++ is the MOST simple editor.
